﻿using System.Collections.Generic;

namespace TwineReader
{
    public class PassageView
    {
        public string Text { get; internal set; } = "";

        public readonly IList<PassageLink> Links = new List<PassageLink>();

        public readonly IList<string> Actions = new List<string>();
    }
}