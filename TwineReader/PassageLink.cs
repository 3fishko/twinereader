﻿namespace TwineReader
{
    public struct PassageLink
    {
        public readonly string Text;
        public readonly Passage Passage;

        public PassageLink(string text, Passage passage)
        {
            Text = text;
            Passage = passage;
        }
    }
}