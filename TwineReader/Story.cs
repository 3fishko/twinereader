﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using NCalc;

namespace TwineReader
{
    public class Story
    {
        public readonly string Name;

        public readonly Passage StartPassage;
        
        internal readonly Dictionary<string, Passage> Passages = new Dictionary<string, Passage>();

        public event EvaluateFunctionHandler EvaluateFunction;
        public event EvaluateParameterHandler EvaluateParameter;

        public Story(string storyPath)
        {
            ReadStoryFile(storyPath, ref Name, ref StartPassage);
        }

        private void ReadStoryFile(string storyPath, ref string name, ref Passage startPassage)
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load(storyPath);
            var xmlStory = htmlDoc.DocumentNode?.Element("tw-storydata") ??
                           htmlDoc.DocumentNode?.Element("html")?.Element("body")?.Element("tw-storydata");
            if (xmlStory == null) return;
            name = xmlStory.GetAttributeValue("name", "");
            int idStartnode = xmlStory.GetAttributeValue("startnode", 1);
            foreach (var xmlPassage in xmlStory.Elements("tw-passagedata"))
            {
                var passname = xmlPassage.GetAttributeValue("name", "").Trim();
                var passtext = System.Net.WebUtility.HtmlDecode(xmlPassage.InnerText);
                var passage = new Passage(passtext);
                Passages.Add(passname, passage);

                var passid = xmlPassage.GetAttributeValue("pid", 0);
                if (passid == idStartnode)
                    startPassage = passage;
            }
            foreach (var pass in Passages.Values)
                pass.Init(this);
        }


        /// <summary>
        /// Вычислить нечто, полученное из истории
        /// </summary>
        /// <param name="action"> действие </param>
        public object Evaluate(string action)
        {
            var expr = new Expression(action);
            expr.EvaluateParameter += EvaluateParameter;
            expr.EvaluateFunction += EvaluateFunction;
            return expr.Evaluate();
        }

        /// <summary>
        /// Проверка условия истории
        /// </summary>
        /// <param name="predicate"> условие </param>
        internal bool Check(string predicate)
        {
            return Convert.ToBoolean(Evaluate(predicate));
        }
    }
}
