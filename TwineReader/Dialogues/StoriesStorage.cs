using System.Collections.Generic;
using System.IO;
using System.Linq;
using NCalc;

namespace TwineReader.Dialogues
{
    /// <summary>
    /// ��������� �������
    /// </summary>
    public class StoriesStorage
    {
        protected internal readonly Dictionary<string, TwineReader.Story> Stories;

        public StoriesStorage(string sourceDir, EvaluateFunctionHandler onEvaluateFunction, EvaluateParameterHandler onEvaluateParameter)
        {
            Stories = Directory.GetFiles(sourceDir, "*.html", SearchOption.AllDirectories)
                .Select(sp =>
                {
                    try
                    {
                        return new TwineReader.Story(sp);
                    }
                    catch
                    {
                        return null;
                    }
                }).Where(s => s != null).ToDictionary(s => s.Name);
            foreach (var story in Stories)
            {
                story.Value.EvaluateFunction += onEvaluateFunction;
                story.Value.EvaluateParameter += onEvaluateParameter;
            }
        }

        public Story this[string name]
        {
            get
            {
                if (string.IsNullOrWhiteSpace(name)) return null;
                Stories.TryGetValue(name, out var story);
                return story;
            }
        }
    }

}