using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NCalc;

namespace TwineReader.Dialogues
{
    /// <summary>
    /// Storage with defined functions
    /// </summary>
    public abstract class StoriesStorageFunction : StoriesStorage
    {
        private readonly Dictionary<string, MethodInfo> _methods = new Dictionary<string, MethodInfo>(StringComparer.InvariantCultureIgnoreCase);
        private readonly Dictionary<string, PropertyInfo> _params = new Dictionary<string, PropertyInfo>(StringComparer.InvariantCultureIgnoreCase);

        protected StoriesStorageFunction(string sourceDir)
            : base(sourceDir, null, null)
        {
            foreach (var story in Stories)
            {
                story.Value.EvaluateFunction += DialoguesOnEvaluateFunction;
                story.Value.EvaluateParameter += DialoguesOnEvaluateParameter;
            }

            foreach (var method in GetType().GetMethods())
            {
                if (method.Attributes.HasFlag(MethodAttributes.Static)) continue;
                var dfattr = method.GetCustomAttributes(false).OfType<StoryFuncAttribute>().FirstOrDefault();
                if (dfattr == null) continue;

                _methods.Add(dfattr.Name ?? method.Name, method);
            }

            foreach (var property in GetType().GetProperties())
            {
                if (property.GetMethod.Attributes.HasFlag(MethodAttributes.Static)) continue;
                var dfattr = property.GetCustomAttributes(false).OfType<StoryParamAttribute>().FirstOrDefault();
                if (dfattr == null) continue;

                _params.Add(dfattr.Name ?? property.Name, property);
            }
        }

        private void DialoguesOnEvaluateFunction(string name, FunctionArgs args)
        {
            if (_methods.TryGetValue(name, out var func))
                args.Result = func.Invoke(this, args.EvaluateParameters());
        }

        private void DialoguesOnEvaluateParameter(string name, ParameterArgs args)
        {
            if (_params.TryGetValue(name, out var param))
                args.Result = param.GetMethod.Invoke(this, null);
        }
    }
}