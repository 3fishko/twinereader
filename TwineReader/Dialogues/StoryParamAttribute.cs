using System;

namespace TwineReader.Dialogues
{
    [AttributeUsage(AttributeTargets.Property)]
    public class StoryParamAttribute : Attribute
    {
        public StoryParamAttribute(string name = null)
        {
            Name = name;
        }

        public string Name { get; }
    }
}