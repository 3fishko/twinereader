using System;

namespace TwineReader.Dialogues
{
    [AttributeUsage(AttributeTargets.Method)]
    public class StoryFuncAttribute : Attribute
    {
        public StoryFuncAttribute(string name = null)
        {
            Name = name;
        }

        public string Name { get; }
    }
}