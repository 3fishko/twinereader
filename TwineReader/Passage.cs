﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace TwineReader
{
    public class Passage
    {
        private readonly string _text;
        private Story _story;
        private PassageView _constState;
        private readonly List<IPassagePart> _passageParts = new List<IPassagePart>();


        public Passage(string passtext)
        {
            _text = passtext.Trim();
        }

        public PassageView GetStateNow()
        {
            if (_constState != null)
            {
                foreach (var stateAction in _constState.Actions)
                    _story.Evaluate(stateAction);

                return _constState;
            }

            var state = new PassageView();
            foreach (var passagePart in _passageParts)
                passagePart.FillState(_story, state);

            foreach (var stateAction in state.Actions)
                _story.Evaluate(stateAction);

            state.Text = state.Text.Trim();
            return state;
        }

        internal void Init(Story story)
        {
            _story = story;

            bool isUnconditional = FindTextParts();
            if (isUnconditional)
            {
                _constState = new PassageView();
                foreach (var passagePart in _passageParts)
                    passagePart.FillState(_story, _constState);
                _constState.Text = _constState.Text.Trim();
                _passageParts.Clear();
            }
        }

        #region Text parsing

        #region IPassagePart

        private interface IPassagePart
        {
            void FillState(Story scriptProcessor, PassageView state);
        }

        [DebuggerDisplay("Text = {_text}")]
        private class TextPassagePart : IPassagePart
        {
            private readonly string _text;

            public TextPassagePart(string text)
            {
                _text = text;
            }

            public void FillState(Story scriptProcessor, PassageView state)
            {
                state.Text += _text;
            }
        }

        [DebuggerDisplay("Predicate = {_predicate}")]
        private class ConditionPassagePart : IPassagePart
        {
            private readonly string _predicate;
            private readonly IList<IPassagePart> _content;
            private readonly IList<IPassagePart> _elseContent;

            public ConditionPassagePart(string predicate, IList<IPassagePart> content, IList<IPassagePart> elseContent)
            {
                _predicate = predicate;
                _content = content;
                _elseContent = elseContent;
            }

            public void FillState(Story scriptProcessor, PassageView state)
            {
                if (scriptProcessor.Check(_predicate))
                {
                    foreach (var passagePart in _content)
                        passagePart.FillState(scriptProcessor, state);
                    return;
                }
                if (_elseContent == null) return;
                foreach (var passagePart in _elseContent)
                    passagePart.FillState(scriptProcessor, state);
            }
        }

        [DebuggerDisplay("Link = {_link.Text}")]
        private class LinkPassagePart : IPassagePart
        {
            private readonly PassageLink _link;

            public LinkPassagePart(PassageLink link)
            {
                _link = link;
            }

            public void FillState(Story scriptProcessor, PassageView state)
            {
                state.Links.Add(_link);
            }
        }

        [DebuggerDisplay("Action = {_action}")]
        private class ActionPassagePart : IPassagePart
        {
            private readonly string _action;

            public ActionPassagePart(string action)
            {
                _action = action;
            }

            public void FillState(Story scriptProcessor, PassageView state)
            {
                state.Actions.Add(_action);
            }
        }

        [DebuggerDisplay("Print = {_action}")]
        private class PrintActionPassagePart : IPassagePart
        {
            private readonly string _action;

            public PrintActionPassagePart(string action)
            {
                _action = action;
            }

            public void FillState(Story scriptProcessor, PassageView state)
            {
                state.Text += scriptProcessor.Evaluate(_action).ToString();
            }
        }

        #endregion
        
        // private static readonly Regex StartIfOpenRegex = new Regex(@"<<\s*if\s*",                                           RegexOptions.Compiled);
        // private static readonly Regex EndIfOpenRegex   = new Regex(@"\s*>>",                                             RegexOptions.Compiled);
        private static readonly Regex IfRegex          = new Regex(@"<<\s*if\s*(.*)\s*>>",                                  RegexOptions.Compiled);
        private static readonly Regex ElseRegex        = new Regex(@"<<\s*else\s*>>",                                            RegexOptions.Compiled);
        private static readonly Regex EndIfRegex       = new Regex(@"<<\s*endif\s*>>",                                           RegexOptions.Compiled);
        private static readonly Regex LinkRegex        = new Regex(@"\[\[.+([(\->)|(<\-)|\|].*)?\]\]",                     RegexOptions.Compiled);
        private static readonly Regex ActionRegex      = new Regex(@"<<\s*\w+\s*\(\s*(\w+,\s*)*(\w+)?\s*\)\s*>>",       RegexOptions.Compiled);
        private static readonly Regex PrintActionRegex = new Regex(@"<<\s*print:\s*\w+\s*\(\s*(\w+,\s*)*(\w+)?\s*\)\s*>>", RegexOptions.Compiled);
        private static readonly Regex StartPrintRegex  = new Regex(@"<<\s*print:\s*", RegexOptions.Compiled);
        
        private bool FindTextParts()
        {
            var ifStartMatches = IfRegex.Matches(_text).OfType<Match>().ToList();
            var ifEndMatches = EndIfRegex.Matches(_text).OfType<Match>().ToList();
            var elseMatch = ElseRegex.Match(_text);
            if (ifStartMatches.Count != ifEndMatches.Count)
            {
                _passageParts.Add(new TextPassagePart(_text));
                return true;
            }
            if (ifStartMatches.Count <= 0)
                return ParseUnconditional(0, _text.Length, _passageParts);

            int iText = 0;
            ParseConditional(ifStartMatches, ref elseMatch, ifEndMatches, ref iText, _passageParts);
            ParseUnconditional(iText, _text.Length, _passageParts);

            return false;
        }

        private void ParseConditional(List<Match> ifStartMatches, ref Match elseMatch, List<Match> ifEndMatches, ref int iText, List<IPassagePart> passageParts)
        {
            while (ifStartMatches.Count > 0)
            {
                var ifStartMatch = ifStartMatches[0];
                ifStartMatches.RemoveAt(0);

                ParseUnconditional(iText, ifStartMatch.Index, passageParts);
                iText = ifStartMatch.Index + ifStartMatch.Length;

                var ifParts = new List<IPassagePart>();
                int ifBlockEndPretendent = elseMatch.Success ? Math.Min(elseMatch.Index, ifEndMatches[0].Index) : ifEndMatches[0].Index;
                while (ifStartMatches.Count > 0 && ifStartMatches[0].Index < ifBlockEndPretendent)
                {
                    // есть вложенный условный блок
                    ParseConditional(ifStartMatches, ref elseMatch, ifEndMatches, ref iText, ifParts);
                    ifBlockEndPretendent = elseMatch.Success ? Math.Min(elseMatch.Index, ifEndMatches[0].Index) : ifEndMatches[0].Index;
                }


                List<IPassagePart> lastParsePartsParts = ifParts;
                List<IPassagePart> elseParts = null;
                // они созданы друг для друга
                if (elseMatch.Success && elseMatch.Index < ifEndMatches[0].Index)
                {
                    ParseUnconditional(iText, ifBlockEndPretendent, ifParts);
                    iText = elseMatch.Index + elseMatch.Length;
                    elseMatch = elseMatch.NextMatch();
                    elseParts = new List<IPassagePart>();
                    lastParsePartsParts = elseParts;

                    while (ifStartMatches.Count > 0 && ifStartMatches[0].Index < ifEndMatches[0].Index)
                    {
                        // есть вложенный условный блок
                        ParseConditional(ifStartMatches, ref elseMatch, ifEndMatches, ref iText, elseParts);
                    }
                    ifBlockEndPretendent = ifEndMatches[0].Index;
                }

                ParseUnconditional(iText, ifBlockEndPretendent, lastParsePartsParts);
                iText = ifEndMatches[0].Index + ifEndMatches[0].Length;
                ifEndMatches.RemoveAt(0);

                var condition = GetCondition(ifStartMatch);
                passageParts.Add(new ConditionPassagePart(condition, ifParts, elseParts));
            }
        }

        private string GetCondition(Match ifStartMatch)
        {
            return ifStartMatch.Groups[1].Value;
            // var condition = StartIfOpenRegex.Replace(ifStartMatch.Value, "");
            // condition = EndIfOpenRegex.Replace(condition, "");
            // return condition.Trim();
        }

        private bool ParseUnconditional(int from, int to, ICollection<IPassagePart> passageParts)
        {
            var text = _text.Substring(from, to - from);
            var actionMatch = ActionRegex.Match(text);
            var printActionMatch = PrintActionRegex.Match(text);
            var linkMatch = LinkRegex.Match(text);
            
            int iAction = actionMatch.Success ? actionMatch.Index : int.MaxValue;
            int iPrintAction = printActionMatch.Success ? printActionMatch.Index : int.MaxValue;
            int iLink = linkMatch.Success ? linkMatch.Index : int.MaxValue;
            int iText = 0;
            bool unconditional = !printActionMatch.Success;


            while (actionMatch.Success || printActionMatch.Success || linkMatch.Success)
            {
                if (iAction < iLink)
                    if (iAction < iPrintAction)
                        iAction = AddPassagePart(passageParts, ref actionMatch, ParseAction, ref iText, text);
                    else
                        iPrintAction = AddPassagePart(passageParts, ref printActionMatch, ParsePrintAction, ref iText, text);
                else if (iLink < iPrintAction)
                    iLink = AddPassagePart(passageParts, ref linkMatch, ParseLink, ref iText, text);
                else
                    iPrintAction = AddPassagePart(passageParts, ref printActionMatch, ParsePrintAction, ref iText, text);
            }

            if (iText < text.Length)
                passageParts.Add(new TextPassagePart(text.Substring(iText)));

            return unconditional;
        }

        private static int AddPassagePart(ICollection<IPassagePart> passageParts, 
            ref Match match, Func<Match, IPassagePart> parser, ref int iText, string text)
        {
            if (iText < match.Index)
                passageParts.Add(new TextPassagePart(text.Substring(iText, match.Index - iText)));
            iText = match.Index + match.Length;

            passageParts.Add(parser(match));
            match = match.NextMatch();
            return match.Success ? match.Index : int.MaxValue;
        }

        private IPassagePart ParseLink(Match match)
        {
            var linkString = match.Value;
            linkString = linkString.Substring(2, linkString.Length - 4).Trim();
            string linkName = linkString;
            string linkText = linkString;

            int delimerIndex = linkString.IndexOf('|');
            if (delimerIndex >= 0)
            {
                linkName = linkString.Substring(delimerIndex + 1).Trim();
                linkText = linkString.Substring(0, delimerIndex).Trim();
            }
            else
            {
                delimerIndex = linkString.IndexOf("->");
                if (delimerIndex >= 0)
                {
                    linkName = linkString.Substring(delimerIndex + 2).Trim();
                    linkText = linkString.Substring(0, delimerIndex).Trim();
                }
                else
                {
                    delimerIndex = linkString.IndexOf("<-");
                    if (delimerIndex >= 0)
                    {
                        linkText = linkString.Substring(delimerIndex + 2).Trim();
                        linkName = linkString.Substring(0, delimerIndex).Trim();
                    }
                }
            }
            return new LinkPassagePart(new PassageLink(linkText, _story.Passages[linkName]));
        }

        private static IPassagePart ParsePrintAction(Match match)
        {
            var actionString = match.Value;
            actionString = StartPrintRegex.Replace(actionString, "");
            actionString = actionString.Substring(0, actionString.Length - 2).Trim();
            return new PrintActionPassagePart(actionString);
        }

        private static IPassagePart ParseAction(Match match)
        {
            var actionString = match.Value;
            actionString = actionString.Substring(2, actionString.Length - 4);
            return new ActionPassagePart(actionString.Trim());
        }
        
        #endregion

    }
}