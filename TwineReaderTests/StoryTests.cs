﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TwineReader.Dialogues;

namespace TwineReaderTests
{
    [TestClass()]
    public class StoryTests
    {
        class TestDialog : StoriesStorageFunction
        {
            public TestDialog(string sourceDir) : base(sourceDir)
            {
            }

            [StoryFunc("KeyExist")]
            public bool KeyExistFunc()
            {
                return KeyExist;
            }

            [StoryFunc]
            public bool UsePicklock()
            {
                if (NPicklock < 3)
                {
                    NPicklock = 0;
                    return false;
                }
                NPicklock -= 3;
                return true;
            }

            [StoryFunc]
            public void OpenChest()
            {
                ChestOpen = true;
            }

            [StoryFunc]
            public void DoCrachChest()
            {
                ChestCracked = true;
            }

            [StoryParam]
            public bool ChestOpen { get; set; }

            [StoryParam]
            public bool ChestCracked { get; set; }

            public bool KeyExist { get; set; }
            
            [StoryParam]
            public int NPicklock { get; set; }
        }

        [TestMethod()]
        public void StoryTest()
        {
            var stories = new TestDialog("./");
            var story = stories["LockedChestStandart"];

            var passage = story.StartPassage.GetStateNow();
            Assert.AreEqual(2, passage.Links.Count);

            stories.KeyExist = true;

            passage = story.StartPassage.GetStateNow();
            Assert.AreEqual(3, passage.Links.Count);

            stories.NPicklock = 1;

            passage = story.StartPassage.GetStateNow();
            Assert.AreEqual(3, passage.Links.Count);
            Assert.AreEqual("Открыть ключом.", passage.Links[0].Text);
            passage.Links[0].Passage.GetStateNow();
            Assert.IsTrue(stories.ChestOpen);

            stories.ChestOpen = false;
            stories.KeyExist = false;

            passage = story.StartPassage.GetStateNow();
            Assert.AreEqual(3, passage.Links.Count);
            Assert.AreEqual("Попытаться взломать.", passage.Links[0].Text);
            passage.Links[0].Passage.GetStateNow();
            Assert.IsFalse(stories.ChestOpen);
            Assert.AreEqual(0, stories.NPicklock);

            stories.NPicklock = 10;
            stories.ChestOpen = false;
            stories.KeyExist = false;

            passage = story.StartPassage.GetStateNow();
            Assert.AreEqual(3, passage.Links.Count);
            Assert.AreEqual("Попытаться взломать.", passage.Links[0].Text);
            passage.Links[0].Passage.GetStateNow();
            Assert.IsTrue(stories.ChestOpen);
            Assert.AreEqual(7, stories.NPicklock);

            passage = story.StartPassage.GetStateNow();
            Assert.AreEqual(3, passage.Links.Count);
            Assert.AreEqual("Попытаться сломать замок.", passage.Links[1].Text);
            passage.Links[1].Passage.GetStateNow();
            Assert.IsTrue(stories.ChestCracked);
        }
    }
}